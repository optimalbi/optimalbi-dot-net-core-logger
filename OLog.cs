// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Text;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Optimal.Logger {
    /// <summary>
    ///     The main static class for logging using the OLogger!
    /// </summary>
    public static class OLog {
        /// <summary>
        ///     The loggers master list.
        /// </summary>
        private static readonly List<IOptimalLogger> Loggers = new List<IOptimalLogger>();

        /// <summary>
        /// True if OLog is configured with at least one logger.
        /// </summary>
        public static bool Configured => Loggers.Count > 0;

        /// <summary>
        ///     Logs an info level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static async Task InfoAsync(string message, params object[] context) {
            Task[] tasks = new Task[Loggers.Count];
            for(int i = 0; i < Loggers.Count; i++) {
                tasks[i] = Loggers[i].InfoAsync(message, context);
            }
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Logs an info level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static void Info(string message, params object[] context) {
            foreach(IOptimalLogger logger in Loggers) {
                logger.Info(message, context);
            }
        }

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static async Task ErrorAsync(string message, params object[] context) {
            Task[] tasks = new Task[Loggers.Count];
            for(int i = 0; i < Loggers.Count; i++) {
                tasks[i] = Loggers[i].ErrorAsync(message, context);
            }
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static void Error(string message, params object[] context) {
            foreach(IOptimalLogger logger in Loggers) {
                logger.Error(message, context);
            }
        }

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="exception">The causing exception.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static async Task ErrorAsync(string message, Exception exception, params object[] context) {
            Task[] tasks = new Task[Loggers.Count];
            for(int i = 0; i < Loggers.Count; i++) {
                tasks[i] = Loggers[i].ErrorAsync(message, exception, context);
            }
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="exception">The causing exception.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static void Error(string message, Exception exception, params object[] context) {
            foreach(IOptimalLogger logger in Loggers) {
                logger.Error(message, exception, context);
            }
        }

        /// <summary>
        ///     Logs a warn level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static async Task WarnAsync(string message, params object[] context) {
            Task[] tasks = new Task[Loggers.Count];
            for(int i = 0; i < Loggers.Count; i++) {
                tasks[i] = Loggers[i].WarnAsync(message, context);
            }
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Logs a warn level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static void Warn(string message, params object[] context) {
            foreach(IOptimalLogger logger in Loggers) {
                logger.Warn(message, context);
            }
        }

        /// <summary>
        ///     Logs a debug level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static async Task DebugAsync(string message, params object[] context) {
            Task[] tasks = new Task[Loggers.Count];
            for(int i = 0; i < Loggers.Count; i++) {
                tasks[i] = Loggers[i].DebugAsync(message, context);
            }
            await Task.WhenAll(tasks);
        }

        /// <summary>
        ///     Logs a debug level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        public static void Debug(string message, params object[] context) {
            foreach(IOptimalLogger logger in Loggers) {
                logger.Debug(message, context);
            }
        }

        /// <summary>
        /// Adds a new logger to the master logger.
        /// </summary>
        /// <param name="logger">Logger to add.</param>
        public static void AddLogger(IOptimalLogger logger) {
            Loggers.Add(logger);
        }

        /// <summary>
        /// Removes a logger from the master logger.
        /// </summary>
        /// <param name="logger">Logger to remove.</param>
        public static void RemoveLogger(IOptimalLogger logger) {
            Loggers.Remove(logger);
        }

        /// <summary>
        /// Counts the loggers in the master logger.
        /// </summary>
        /// <returns>The count of loggers.</returns>
        public static int CountLoggers() {
            return Loggers.Count;
        }

        /// <summary>
        ///     Resets the logger to the unconfigured state. Will delete all loggers.
        /// </summary>
        public static void ResetLoggers() {
            Loggers.Clear();
        }

        /// <summary>
        ///     Gets an immutable enumerable of the current loggers.
        /// </summary>
        /// <returns>Enumerable of the current in use loggers.</returns>
        public static IEnumerable<IOptimalLogger> GetLoggers() {
            return Loggers.ToImmutableList();
        }

        /// <summary>
        ///     Helper for appending a list of context objects to a StringBuilder, separated by spaces, or an optionally specified separator.
        /// </summary>
        /// <param name="context">Array of objects to append.</param>
        /// <param name="sb">StringBuilder to append to.</param>
        /// <param name="separator">The separator to use.</param>
        public static void AppendFromContext(object[] context, [CanBeNull] ref StringBuilder sb, string separator = " ") {
            if(sb == null) {
                sb = new StringBuilder();
            }
            if(context.Length == 0) {
                return;
            }
            foreach(object t in context) {
                sb.Append(separator);
                if(t == null) {
                    sb.Append("null");
                } else {
                    sb.Append(t);
                }
            }
        }

        /// <summary>
        ///     Creates a string from a list of objects.
        /// </summary>
        /// <param name="context">The array of context objects.</param>
        /// <param name="separator">The string to use to separate the objects.</param>
        /// <returns>String representation of the object list.</returns>
        /// <exception cref="System.NullReferenceException">Method OLog.AppendFromContext(context, ref sb) set sb to null</exception>
        [NotNull]
        public static string GetStringFromContext(object[] context, string separator) {
            StringBuilder sb = null;
            AppendFromContext(context, ref sb, separator);
            if(sb == null) {
                throw new NullReferenceException("Method OLog.AppendFromContext(context, ref sb) set sb to null");
            }
            return sb.ToString();
        }
    }
}