﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.Threading.Tasks;

namespace Optimal.Logger {
    /// <summary>
    /// Interface for a logger. Loggers write to specified outputs at different log levels.
    /// </summary>
    public interface IOptimalLogger {
        /// <summary>
        /// Minimum level this logger writes at.
        /// </summary>
        LogLevel Level { get; set; }

        /// <summary>
        ///     Logs an info level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        /// <returns></returns>
        Task InfoAsync(string message, params object[] context);

        /// <summary>
        ///     Logs an info level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        void Info(string message, params object[] context);

        /// <summary>
        ///     Logs a debug level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        /// <returns></returns>
        Task DebugAsync(string message, params object[] context);

        /// <summary>
        ///     Logs a debug level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        void Debug(string message, params object[] context);

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        /// <returns></returns>
        Task ErrorAsync(string message, params object[] context);

        /// <summary>
        ///     Logs an error level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        void Error(string message, params object[] context);

        /// <summary>
        ///     Logs an error level event with a causing exception.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="exception">A causing exception</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        /// <returns></returns>
        Task ErrorAsync(string message, Exception exception, params object[] context);

        /// <summary>
        ///     Logs an error level event with a causing exception.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="exception">A causing exception</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        void Error(string message, Exception exception, params object[] context);

        /// <summary>
        ///     Logs a warn level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        /// <returns></returns>
        Task WarnAsync(string message, params object[] context);

        /// <summary>
        ///     Logs a warn level event.
        /// </summary>
        /// <param name="message">The English description of the event.</param>
        /// <param name="context">Optional objects that are relevant for the event.</param>
        void Warn(string message, params object[] context);
    }
}