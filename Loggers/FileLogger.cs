﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.IO;
using System.Threading.Tasks;

namespace Optimal.Logger.Loggers{
    /// <summary>
    /// Logger which writes to a file.
    /// </summary>
    public class FileLogger : OptimalLogger{
        private readonly TextWriter fileStream;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="level">The minimum level of events we should log.</param>
        /// <param name="file">The file to write to.</param>
        public FileLogger(LogLevel level, string file) : base(level){
            fileStream =
                new StreamWriter(!File.Exists(file)
                    ? File.Create(file, 4, FileOptions.Asynchronous)
                    : File.Open(file, FileMode.Append, FileAccess.Write, FileShare.ReadWrite));
        }

        /// <inheritdoc />
        public override async Task Write(LogLevel level, string formatted, string message, Exception exception,
            params object[] context){
            await fileStream.WriteLineAsync(formatted);
            await fileStream.FlushAsync();
        }
    }
}