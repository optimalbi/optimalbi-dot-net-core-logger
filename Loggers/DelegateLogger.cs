﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Optimal.Logger.Loggers {
    /// <summary>
    /// Logger for providing custom logic when you don't want to inherit the OptimalLogger class.
    /// </summary>
    public class DelegateLogger : OptimalLogger {
        private readonly LogWrite logWrite;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="level">The minimum level of events we should log.</param>
        /// <param name="logWriter">The delegate to use to log.</param>
        public DelegateLogger(LogLevel level, LogWrite logWriter) : base(level) {
            if(logWriter == null) {
                throw new NullReferenceException($"{nameof(logWriter)} was null");
            }
            this.logWrite = logWriter;
        }

        /// <summary>
        /// Delegate to satisfy the requirements for log writing. Should behave synchronously if possible.
        /// </summary>
        /// <param name="level">The level of this log event.</param>
        /// <param name="formatted">The text post formatting (Recommended to print).</param>
        /// <param name="message">The raw preformatted message</param>
        /// <param name="exception">The causing exception if applicable.</param>
        /// <param name="context">Context objects.</param>
        public delegate void LogWrite(
            LogLevel level, string formatted, string message, Exception exception, params object[] context);

        /// <inheritdoc />
        public override async Task Write(LogLevel level, string formatted, string message, Exception exception,
            params object[] context) {
            await Task.Run(() => logWrite(level, formatted, message, exception, context));
        }
    }
}