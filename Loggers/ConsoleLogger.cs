﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.IO;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace Optimal.Logger.Loggers {
    /// <summary>
    ///     Logger to log to console or other text based streams.
    /// </summary>
    public class ConsoleLogger : OptimalLogger {
        private readonly TextWriter error;
        private readonly TextWriter writer;

        /// <summary>
        ///     Default constructor for Console logger that outputs to the default console output.
        /// </summary>
        public ConsoleLogger() : base(LogLevel.Info) {
            writer = Console.Out;
            error = Console.Error;
        }

        /// <summary>
        ///     Constructor with inputs for a custom TextWriter to write to.
        /// </summary>
        /// <param name="output">The TextWriter to write to.</param>
        public ConsoleLogger([NotNull] TextWriter output) : base(LogLevel.Info) {
            writer = output;
            error = output;
            this.SetupStreams();
        }

        /// <summary>
        ///     Constructor where you can specify both an output and a separate error stream.
        /// </summary>
        /// <param name="output">Standard Output Stream</param>
        /// <param name="error">Error Output Stream</param>
        public ConsoleLogger([NotNull] TextWriter output, [NotNull] TextWriter error) : base(LogLevel.Info) {
            writer = output;
            this.error = error;
            this.SetupStreams();
        }

        /// <summary>
        ///     Can specify both an output stream and an optional error stream.
        /// </summary>
        /// <param name="output">Standard Output Stream</param>
        /// <param name="error">Error Output Stream</param>
        public ConsoleLogger([NotNull] Stream output, [CanBeNull] Stream error = null) : base(LogLevel.Info) {
            writer = new StreamWriter(output);
            this.error = error != null ? new StreamWriter(error) : new StreamWriter(output);
            this.SetupStreams();
        }

        /// <summary>
        ///     Makes sure the streams are in a good state to write to.
        /// </summary>
        private void SetupStreams() {
            writer.Flush();
            error.Flush();
        }

        /// <inheritdoc />
        public override async Task Write(LogLevel level, string formatted, string message, Exception exception,
            params object[] context) {
            if(level > LogLevel.Warn) {
                await error.WriteLineAsync(formatted);
                await error.FlushAsync();
            } else {
                await writer.WriteLineAsync(formatted);
                await writer.FlushAsync();
            }
        }
    }
}