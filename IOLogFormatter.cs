﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using JetBrains.Annotations;

namespace Optimal.Logger {
    // ReSharper disable once InconsistentNaming
    /// <summary>
    /// Utility for formatting text.
    /// </summary>
    public interface IOLogFormatter {
        /// <summary>
        /// Take a log event and return the formatted text for output.
        /// </summary>
        /// <param name="message">The raw text of the message.</param>
        /// <param name="exception">The causing exception, or null if there is none.</param>
        /// <param name="level">The log level of this event.</param>
        /// <param name="context">The array of context objects provided for this log event.</param>
        /// <returns></returns>
        string FormatText([NotNull] string message, [CanBeNull] Exception exception, LogLevel level,
            params object[] context);
    }
}