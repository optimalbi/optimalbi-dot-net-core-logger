﻿// Timothy Gray
// 0203
// 2017020311:22 AM

namespace Optimal.Logger {
    public enum LogLevel {
        Debug = 0,
        Info = 1,
        Warn = 2,
        Error = 3
    }
}