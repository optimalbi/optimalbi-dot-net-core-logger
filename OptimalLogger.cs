﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.Threading.Tasks;

namespace Optimal.Logger {
    /// <summary>
    /// Default logger class.
    /// </summary>
    public abstract class OptimalLogger : IOptimalLogger {
        protected OptimalLogger(LogLevel level) {
            Level = level;
        }
        
        public virtual IOLogFormatter Formatter { get; set; } = new DefaultOLogFormatter();

        /// <inheritdoc />
        public LogLevel Level { get; set; }

        /// <inheritdoc />
        public async Task InfoAsync(string message, params object[] context) {
            await this.PreWrite(LogLevel.Info, message, null, context);
        }

        /// <inheritdoc />
        public void Info(string message, params object[] context) {
            Task task = this.InfoAsync(message, context);
            Task.WaitAll(task);
        }

        /// <inheritdoc />
        public async Task DebugAsync(string message, params object[] context) {
            await this.PreWrite(LogLevel.Debug, message, null, context);
        }

        /// <inheritdoc />
        public void Debug(string message, params object[] context) {
            Task task = this.DebugAsync(message, context);
            Task.WaitAll(task);
        }

        /// <inheritdoc />
        public async Task ErrorAsync(string message, params object[] context) {
            await this.PreWrite(LogLevel.Error, message, null, context);
        }

        /// <inheritdoc />
        public void Error(string message, params object[] context) {
            Task task = this.ErrorAsync(message, context);
            Task.WaitAll(task);
        }

        /// <inheritdoc />
        public async Task ErrorAsync(string message, Exception exception, params object[] context) {
            await this.PreWrite(LogLevel.Error, message, exception, context);
        }

        /// <inheritdoc />
        public void Error(string message, Exception exception, params object[] context) {
            Task task = this.ErrorAsync(message, exception, context);
            Task.WaitAll(task);
        }

        /// <inheritdoc />
        public async Task WarnAsync(string message, params object[] context) {
            await this.PreWrite(LogLevel.Warn, message, null, context);
        }

        /// <inheritdoc />
        public void Warn(string message, params object[] context) {
            Task task = this.WarnAsync(message, context);
            Task.WaitAll(task);
        }

        /// <summary>
        /// Formats message and writes it to output.
        /// </summary>
        /// <param name="level">The level of this log event.</param>
        /// <param name="message">The raw preformatted message</param>
        /// <param name="exception">The causing exception if applicable.</param>
        /// <param name="context">Context objects.</param>
        /// <returns></returns>
        private async Task PreWrite(LogLevel level, string message, Exception exception, params object[] context) {
            if(level < Level) {
                return;
            }
            string formatted = Formatter.FormatText(message, exception, level, context);
            await this.Write(level, formatted, message, exception, context);
        }

        /// <summary>
        /// Write to this logger's specified output.
        /// </summary>
        /// <param name="level">The level of this log event.</param>
        /// <param name="formatted">The text post formatting (Recommended to print).</param>
        /// <param name="message">The raw preformatted message</param>
        /// <param name="exception">The causing exception if applicable.</param>
        /// <param name="context">Context objects.</param>
        /// <returns></returns>
        public abstract Task Write(LogLevel level, string formatted, string message, Exception exception,
            params object[] context);
    }
}