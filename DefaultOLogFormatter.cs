﻿// Timothy Gray
// 0203
// 2017020311:22 AM

using System;
using System.Text;

namespace Optimal.Logger {
    /// <summary>
    /// Default formatter for OLog.
    /// </summary>
    public class DefaultOLogFormatter : IOLogFormatter {
        /// <inheritdoc />
        public string FormatText(string message, Exception exception, LogLevel level, params object[] context) {
            StringBuilder sb = new StringBuilder();
            sb.Append($"{DateTime.UtcNow:u} - [{level}] - {message}");
            OLog.AppendFromContext(context, ref sb, " ");
            if(sb == null) {
                throw new NullReferenceException("String builder was null after AppendFromContext");
            }
            if(exception != null) {
                sb.Append(exception.Message);
#if DEBUG
                sb.Append(exception.StackTrace);
#endif
            }
            return sb.ToString();
        }
    }
}